'use strict';

/**
 * @ngdoc overview
 * @name amhBlog
 * @description # amhBlog
 * 
 * Main module of the application.
 */
angular.module('amhBlog', [
	'ngMaterialHomeBank',
	'ngMaterialHomeSpa',
	'mblowfish-language',
])//
.config(function($localStorageProvider) {
	$localStorageProvider.setKeyPrefix('amhBlog.');
})
.run(function ($app, $toolbar, $sidenav) {
	// start the application
	$toolbar.setDefaultToolbars(['amh.owner-toolbar']);
	$sidenav.setDefaultSidenavs(['amh.main-sidenav', 'amh.cms.pages.sidenav']);
	$app.start('app-amh-blog');
});
