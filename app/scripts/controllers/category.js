/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhBlog')

/**
 * @ngdoc controller
 * @name siteoncloudsHome.controller:CategoryCtrl
 * @description # CategoryCtrl Controller of the siteoncloudsHome
 * 
 */
.controller('CategoryCtrl', function ($scope, $rootScope, $shop, PaginatorParameter,
		$routeParams, $q, $translate, $navigator, $cms, $actions) {

	var ctrl = {};

	$scope.loadingCategories = false;
	$scope.loadingServices = false;
	$scope.categoryNothingFound = false;

	var catId = $routeParams.categoryId ? $routeParams.categoryId : '0';
	var category = null;

	var lastCategoriesPage = null;
	var lastServicesPage = null;

	$scope.categories = [];
	$scope.services = [];

	var pp = new PaginatorParameter();
	pp.setOrder('id', 'd');
	pp.setFilter('parent', catId);

	var servicesPP = new PaginatorParameter();
	servicesPP.setOrder('id', 'd');

	function loadNextCategories() {

		if ($scope.loadingCategories) {
			return;
		}
		if (lastCategoriesPage && !lastCategoriesPage.hasMore()) {
			return;
		}
		if (lastCategoriesPage) {
			pp.setPage(lastCategoriesPage.next());
		} else {
			pp.setPage(1);
		}
		$scope.loadingCategories = true;

		$shop.categories(pp)
		.then(function (subCats) {
			if (subCats.length === 0) {
				$scope.categoryNothingFound = true;
			}
			$scope.categories = $scope.categories.concat(subCats.items);
			lastCategoriesPage = subCats;
			return subCats;
		}, function () {
			alert($translate.instant('Failed to get items.'));
		})
		.finally(function () {
			$scope.loadingCategories = false;
		});

	}

	function loadNextServices() {

		if (!category) {
			return;
		}
		if ($scope.loadingServices) {
			return;
		}
		if (lastServicesPage && !lastServicesPage.hasMore()) {
			return;
		}
		if (lastServicesPage) {
			servicesPP.setPage(lastServicesPage.next());
		} else {
			servicesPP.setPage(1);
		}
		$scope.loadingServices = true;

		// find services of a category
		category.services(servicesPP)//
		.then(function (servicesList) {
			$scope.services = $scope.services.concat(servicesList.items);
			lastServicesPage = servicesList;
			return;
		}, function () {
			alert($translate.instant('Failed to get items.'));
		})//
		.finally(function () {
			$scope.loadingServices = false;
		});
	}

	function goToCategoryPath(path){
		$navigator.openPage(path);
	}
	function submitToOrder(service) {//send an array of serivces id to a new controller to form an order.
		$navigator.openPage('shop/create-order' , {servicesId : [service.id]});
	}

	function loadContentOfCategory() {
		if (catId === '0' || !category || !category.content || ctrl.loadingContent) {
			return;
		}
		ctrl.loadingContent = true;
		return $cms.content(category.content)//
		.then(function (nc) {
			$scope.content = nc;
			if (typeof nc !== 'undefined') {
				nc.value()//
				.then(function (val) {
					$scope.contentValue = val;
				});
			}
		})//
		.finally(function () {
			ctrl.loadingContent = false;
		});
	}

	/**
	 * Save or update content
	 * @returns
	 */
	function saveContent() {
		var job = $scope.content ? _saveContent : _createContent;
		return job();
	}

	function _saveContent() {
		return $scope.content.setValue($scope.contentValue)//
		.then(function () {
			toast('Content is saved successfully');
		}, function () {
			alert('Saving content is failed.');
		});
	}

	function _createContent() {
		var contentName = 'siteonclouds-home-category-content-' + category.id;
		var data = {
				name: contentName,
				title: 'Content of category ' + category.title + '[' + category.id + ']',
				description: 'Content created from SiteOnClouds-Home SPA.',
				mime_type: 'application/json',
				file_name: contentName + '.json'
		};
		$cms.newContent(data)//
		.then(function (myContent) {
			$scope.content = myContent;
			return $scope.content.setValue($scope.contentValue);
		}, function () {
			alert('Failed to create content.');
		})//
		.then(function () {
			category.content = $scope.content.id;
			return category.update();
		})
		.then(function () {
			toast('Content is saved successfully');
		}, function () {
			alert('Saving content is failed.');
		});
	}

	function toggleEditable() {
		$scope.ngEditable = !$scope.ngEditable;
	}

	function load() {
		if (catId === '0') {
			loadNextCategories();
		} else {
			$shop.category(catId)
			.then(function (cat) {
				category = cat;
				$scope.parentCategory = cat;
				return $q.all([loadContentOfCategory(), loadNextCategories(), loadNextServices()]);
			});
		}

	}

	/*
	 * Add scope menus
	 */
	$actions.newAction({// edit content
		id: 'amhsdp.scope.edit_content',
		priority: 15,
		icon: 'edit',
		label: 'Edit content',
		title: 'Edit content',
		description: 'Toggle edit mode of the current contet',
		visible: function () {
			return catId !== '0' && $rootScope.app.user.owner;
		},
		action: toggleEditable,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});

	$actions.newAction({// add new content
		id: 'amhsdp.scope.new_content',
		priority: 15,
		icon: 'add_box',
		label: 'Create new content',
		title: 'Create new content',
		description: 'Create new content',
		visible: function () {
			return catId !== '0' && $rootScope.app.user.owner && (!ctrl.loadingContent && !$scope.content);
		},
		action: _createContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});

	$actions.newAction({// save content
		id: 'amhsdp.scope.save_content',
		priority: 10,
		icon: 'save',
		label: 'Save current changes',
		title: 'Save current changes',
		description: 'Save current changes',
		visible: function () {
			return catId !== '0' && $rootScope.app.user.owner;
		},
		action: saveContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});

	$scope.contentValue = {};
	$scope.ctrl = ctrl;
	$scope.submitToOrder = submitToOrder;
	$scope.goToCategoryPath = goToCategoryPath;
	$scope.nextCategories = loadNextCategories;
	$scope.loadNextServices = loadNextServices;
	$scope.load = load;
	$scope.parentCategory = category;

	load();
});
